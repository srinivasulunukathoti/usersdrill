//Find all users staying in Germany.
function usersStayingInGermany(users) {
    const usersInGermany = Object.keys(users).filter((user) =>{
        return users[user].nationality==="Germany" && user;
    });
    return usersInGermany;
};
module.exports =usersStayingInGermany;