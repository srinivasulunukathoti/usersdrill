const users = require('../usersData');
const groupUsersBasedOnPrograminglangauge = require('../groupUsersBasedOnPrograminglangauge');

try {
    const result = groupUsersBasedOnPrograminglangauge(users);
    console.log(result);
} catch (error) {
    console.error(error);
}