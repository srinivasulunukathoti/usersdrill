//Find all users with masters Degree.
function usersWithMastersDegree(users) {
    const usersMastersDegree = Object.keys(users).filter((user) =>{
        return users[user].qualification === "Masters" && user;
    });
    return usersMastersDegree;
};
module.exports = usersWithMastersDegree;
