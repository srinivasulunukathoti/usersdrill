 //Find all users who are interested in playing video games.
 function whoArePlayingVideoGames(users) {
    const videoGameUsers = Object.keys(users).filter((user) => {
        const interests = users[user]?.interests; // Using optional chaining to avoid errors if 'interests' is undefined
        return interests && (interests.includes("Video Games") || interests.includes("Playing Video Games"));
    });
   return videoGameUsers;
}
module.exports = whoArePlayingVideoGames;