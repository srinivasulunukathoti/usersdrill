// Group users based on their Programming language mentioned in their designation.

function groupUsersBasedOnProgrammingLangauge(users) {
    const groupUsers = Object.keys(users).reduce((accumulator , current) =>{
        if (!accumulator[users[current].desgination]) {
            accumulator[users[current].desgination] =[];
        }
        accumulator[users[current].desgination].push(current.desgination);
        return accumulator;
    },{});
    return groupUsers;
};
module.exports = groupUsersBasedOnProgrammingLangauge;